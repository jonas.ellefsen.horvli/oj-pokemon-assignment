FROM node:lts-alpine AS builder

ENV PORT 3000

WORKDIR /src
COPY package.json .
RUN npm install
COPY . .

RUN npm run build
# /src/dist
FROM node:lts-alpine AS production

EXPOSE 3000/tcp

WORKDIR /app
COPY --from=builder /src/package.json .
RUN npm install express path compression
COPY index.js .
COPY --from=builder /src/dist ./dist

CMD ["node", "index.js"]

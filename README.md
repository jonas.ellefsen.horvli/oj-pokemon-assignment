# OjPokemonAssignment

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.3.24.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

## Components

- **Dashboard**
Entrypoint for the PokemonList and displays the "Load More" button.

- **PokemonList**
Displays the searchbar and the list of Pokemon. Entrypoint for the PokemonListItem.

- **PokemonListItem**
An item for a single pokemon that is used to display in the PokemonList component.

- **PokemonDetail**
Display details information of a single Pokemon. Entrypoint for PokemonProfile, PokemonStats, PokemonSprites and PokemonMoves.

- **PokemonProfile**
A simple component with an Input that displays the Pokemon basic information
  - Height
  - Weight
  - Abilities
  - Base Stats
  - Sprites
  - Moves

- **PokemonStats**
A simple component with an Input to display the Pokemon’s basic stats
  - Weight
  - Height
  - Base Stats
  - Abilities

- **PokemonSprites**
A component with an Input that display all the different sprites a Pokemon could have.

- **PokemonMoves**
A component with an Input that list all the the moves a Pokemon has available.

- **PokemonApiService**
A service that controls all calls to api.

- **PokemonStateService**
A service that manages the app state and keeps track of all pokemon.

## Authors
- **Jonas Horvli**
- **Ole Baadshaug**

'use strict';
/**
 * Dependencies
 * @ignore
 */
const express = require("express");
const compression = require("compression");

const _app_folder = 'dist/oj-pokemon-assignment';

/**
 * App
 * @ignore
 */
const app = express();
app.use(compression());

// ---- SERVE STATIC FILES ---- //
app.get('*.*', express.static(_app_folder, {maxAge: '1y'}));

// ---- SERVE APLICATION PATHS ---- //
app.all('*', function (req, res) {
  res.status(200).sendFile(`/`, {root: _app_folder});
});


// listen
app.listen(process.env.PORT || 3000, () =>
  console.log(`Listening on port ${process.env.PORT || 3000}`)
);

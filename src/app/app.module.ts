import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {HttpClientModule} from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { PokemonListComponent } from './pokemon-list/pokemon-list.component';
import { PokemonListItemComponent } from './pokemon-list-item/pokemon-list-item.component';
import { PokemonDetailComponent } from './pokemon-detail/pokemon-detail.component';
import { PokemonProfileComponent } from './pokemon-profile/pokemon-profile.component';
import { PokemonStatsComponent } from './pokemon-stats/pokemon-stats.component';
import { PokemonMovesComponent } from './pokemon-moves/pokemon-moves.component';
import { AppRoutingModule } from './app-routing.module';
import { PokemonSpritesComponent } from './pokemon-sprites/pokemon-sprites.component';

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    PokemonListComponent,
    PokemonListItemComponent,
    PokemonDetailComponent,
    PokemonProfileComponent,
    PokemonStatsComponent,
    PokemonMovesComponent,
    PokemonSpritesComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

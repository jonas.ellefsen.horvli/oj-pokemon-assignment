import {Pokemon} from './pokemon';
import {Ability, PokemonDetailsResponse, Stat} from './pokemon-helper';
import * as _ from 'lodash';

/**
 * A extended class of pokemon containing all the variables used in app.
 */
export class DetailedPokemon extends Pokemon {
  types: string[];
  baseStats: Stat[];
  height: number;
  weight: number;
  abilities: Ability[];
  baseExperience: number;
  moves: string[];
  image: string;
  sprites: string[];
  constructor(pokemon: PokemonDetailsResponse) {
    super(pokemon.id, pokemon.name);
    this.height = pokemon.height;
    this.weight = pokemon.weight;
    this.types = pokemon.types.map(type => type.type.name);
    this.abilities = _.sortBy(pokemon.abilities, ability => ability.slot);
    this.baseExperience = pokemon.base_experience;
    this.image = pokemon.sprites.front_default;
    this.baseStats = pokemon.stats;
    this.moves = pokemon.moves.map(move => move.move.name);
    this.sprites = Object.values(pokemon.sprites).filter(sprite => sprite);
  }
}

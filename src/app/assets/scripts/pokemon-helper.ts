/**
 * A list of interfaces to keep track of what we get from pokemon api
 */
export interface Stat {
  base_stat: number;
  effort: number;
  stat: { name: string, url: string };
}
export interface Ability {
  ability: { name: string, url: string };
  is_hidden: boolean;
  slot: number;
}
export interface Type {
  slot: number;
  type: { name: string; url: string };
}
export interface Move {
  move: {
    name: string;
    url: string;
  };
}
export interface PokemonResponse {
  count: number;
  next: string;
  previous: string | null;
  results: PokemonResult[];
}
export interface PokemonResult {
  name: string;
  url: string;
}
export interface PokemonDetailsResponse {
  id: number;
  name: string;
  weight: number;
  height: number;
  base_experience: number;
  types: Type[];
  abilities: Ability[];
  stats: Stat[];
  sprites: {
    back_default: string | null;
    back_female: string | null;
    back_shiny: string | null;
    back_shiny_female: string | null;
    front_default: string;
    front_female: string | null;
    front_shiny: string | null;
    front_shiny_female: string | null;
  };
  moves: Move[];
}

/**
 * A simple pokemon used as an object when fetching list of pokemon before fetching details.
 */
export class Pokemon {
  constructor(id: number, name: string) {
    this.id = id;
    this.name = name;
  }
  id: number;
  name: string;
}

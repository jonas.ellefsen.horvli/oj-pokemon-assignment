import { Component, OnInit } from '@angular/core';
import {PokemonStateService} from '../pokemon-state.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  constructor(
    private pokemonService: PokemonStateService
  ) { }

  ngOnInit() {
  }

  loadMorePokemon() {
    this.pokemonService.getPokemonList();
  }
}

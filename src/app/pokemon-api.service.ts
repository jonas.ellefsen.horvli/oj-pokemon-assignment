import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {PokemonDetailsResponse, PokemonResponse} from './assets/scripts/pokemon-helper';

@Injectable({
  providedIn: 'root'
})
/**
 * Service for providing all calls to Pokemon Api.
 */
export class PokemonApiService {
  private apiUrl = 'https://pokeapi.co/api/v2/pokemon';
  private nextPokemonListApiUrl = this.apiUrl;
  constructor(private http: HttpClient) { }

  /**
   * Gets list of pokemon from api.
   */
  getPokemonList(): Observable<PokemonResponse> {
    try {
      const requestResponse = this.http.get<PokemonResponse>(this.nextPokemonListApiUrl);
      requestResponse.subscribe(response => this.nextPokemonListApiUrl = response.next);
      return requestResponse;
    } catch (e) {
      console.error(`Failed on getCatchThemAll(). Error: ${e.message}`);
    }
  }

  /**
   * Gets details of given pokemon id.
   * @param id
   */
  getPokemonDetails(id: number): Observable<PokemonDetailsResponse> {
    try {
      return this.http.get<PokemonDetailsResponse>(`${this.apiUrl}/${id}`);
    } catch (e) {
      console.error(`Failed on getPokemon(). Error: ${e.message}`);
    }
  }
}

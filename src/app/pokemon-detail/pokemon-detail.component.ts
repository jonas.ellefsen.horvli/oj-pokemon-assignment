import {Component, OnInit, OnDestroy } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {PokemonStateService} from '../pokemon-state.service';
import {DetailedPokemon} from '../assets/scripts/detailed-pokemon';

@Component({
  selector: 'app-pokemon-detail',
  templateUrl: './pokemon-detail.component.html',
  styleUrls: ['./pokemon-detail.component.scss']
})
export class PokemonDetailComponent implements OnInit, OnDestroy {
  id;
  pokemon: DetailedPokemon;
  private pokemonSub;
  constructor(private pokemonService: PokemonStateService, private route: ActivatedRoute) {
    this.id = +this.route.snapshot.paramMap.get('id');
    this.pokemonSub = pokemonService.pokemonList$.subscribe(pokemonList => {
      this.pokemon = (pokemonList.find(pokemon => pokemon.id === this.id) as DetailedPokemon);
    });
  }

  ngOnInit() {
    const newPokemon = this.pokemonService.pokemonList.find(pokemon => pokemon.id === this.id);
    if (newPokemon && (newPokemon as DetailedPokemon).image) {
      this.pokemon = (newPokemon as DetailedPokemon);
    } else {
      this.pokemonService.getPokemonDetails(this.id);
    }
  }

  ngOnDestroy(): void {
    this.pokemonSub.unsubscribe();
  }
}

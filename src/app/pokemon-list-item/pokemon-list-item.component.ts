import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PokemonStateService} from '../pokemon-state.service';
import {DetailedPokemon} from '../assets/scripts/detailed-pokemon';

@Component({
  selector: 'app-pokemon-list-item',
  templateUrl: './pokemon-list-item.component.html',
  styleUrls: ['./pokemon-list-item.component.scss']
})
export class PokemonListItemComponent implements OnInit {
  @Input() pokemon: DetailedPokemon;

  constructor(
    private route: ActivatedRoute,
    private pokemonService: PokemonStateService
  ) {
  }

  ngOnInit() {
    if (!this.pokemon.image) {
      this.getPokemon();
    }
  }

  getPokemon(): void {
    this.pokemonService.getPokemonDetails(this.pokemon.id);
  }

}

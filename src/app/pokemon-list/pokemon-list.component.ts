import {Component, OnInit } from '@angular/core';
import {PokemonStateService} from '../pokemon-state.service';


@Component({
  selector: 'app-pokemon-list',
  templateUrl: './pokemon-list.component.html',
  styleUrls: ['./pokemon-list.component.scss']
})
export class PokemonListComponent implements OnInit {
  private pokemonList;
  private searchResult;
  searchText: string;

  constructor(
    private pokemonService: PokemonStateService,
  ) {
    pokemonService.pokemonList$.subscribe(pokemonList => {
      this.pokemonList = pokemonList.slice(0, pokemonService.loadedPokemon);
      /* Initialize the list of displayed pokemon as equal to all loaded pokemon */
      this.searchResult = pokemonList.slice(0, pokemonService.loadedPokemon);
    });
  }

  ngOnInit() {
    if (this.pokemonService.loadedPokemon === 0) {
      this.pokemonService.getPokemonList();
    }
  }

  /**
   * Search algoritm updating the list of displayed pokemon as the user types in query
   */
  searchPokemon(): void {
    this.searchResult = this.pokemonList.filter( p => {
      return p.name.includes(this.searchText);
    });
  }
}

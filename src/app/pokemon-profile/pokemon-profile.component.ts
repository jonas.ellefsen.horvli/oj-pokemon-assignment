import {Component, Input, OnInit} from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-pokemon-profile',
  templateUrl: './pokemon-profile.component.html',
  styleUrls: ['./pokemon-profile.component.scss']
})
export class PokemonProfileComponent implements OnInit {
  @Input() name: string;
  @Input() types: string[];
  constructor(private router: Router) { }

  ngOnInit() {
  }

  backButtonClick = function() {
    this.router.navigateByUrl('/');
  };
}

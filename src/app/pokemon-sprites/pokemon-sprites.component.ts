import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-pokemon-sprites',
  templateUrl: './pokemon-sprites.component.html',
  styleUrls: ['./pokemon-sprites.component.scss']
})
export class PokemonSpritesComponent implements OnInit {
  @Input() sprites: string[];
  constructor() { }

  ngOnInit() {
  }

}

import { TestBed } from '@angular/core/testing';

import { PokemonStateService } from './pokemon-state.service';

describe('PokemonService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PokemonStateService = TestBed.get(PokemonStateService);
    expect(service).toBeTruthy();
  });
});

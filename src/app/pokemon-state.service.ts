import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {Pokemon} from './assets/scripts/pokemon';
import {PokemonApiService} from './pokemon-api.service';
import { sortedUniqBy, sortBy } from 'lodash';
import {DetailedPokemon} from './assets/scripts/detailed-pokemon';

@Injectable({
  providedIn: 'root'
})
/**
 * Service that keeps and provides all pokemon fetched through the Pokemon api service.
 */
export class PokemonStateService {
  private apiUrl = 'https://pokeapi.co/api/v2/pokemon';
  private nextPokemonListApiUrl = this.apiUrl; // The next api call to fetch more pokemon to the list.
  loadedPokemon = 0; // Store the number of pokemon fetched from api so we can show them in order if any others were fetched through getPokemonDetails() ahead of time.
  private readonly _pokemonList = new BehaviorSubject<Pokemon[]>([]); // A behavior subject so we can track the stream of getPokemonList requests.
  readonly pokemonList$ = this._pokemonList.asObservable(); // The pokemon list we provide asObservable to hide the stream source "_pokemonList".

  get pokemonList(): Pokemon[] {
    return this._pokemonList.getValue();
  }
  set pokemonList(pokemonList: Pokemon[]) {
    this._pokemonList.next(pokemonList);
  }

  constructor(private http: HttpClient, private pokemonApiService: PokemonApiService) {
  }

  /**
   * Tells pokemonApiService to get pokemon from api, and stores the result in state.
   * Sorts and makes sure every pokemon in the list is unique.
   */
  getPokemonList() {
    this.pokemonApiService.getPokemonList().subscribe(pokemonList => {
      let newPokemonListItems = pokemonList.results.map((pokemon, i) => new Pokemon(i + this.loadedPokemon + 1, pokemon.name));
      newPokemonListItems = sortedUniqBy(sortBy(
        this.pokemonList.concat(newPokemonListItems), pokemon => pokemon.id), pokemon => pokemon.id);
      this.loadedPokemon += 20;
      this.nextPokemonListApiUrl = pokemonList.next;
      this.pokemonList = newPokemonListItems;
    });
  }

  /**
   * Tells pokemonApiService to get specific pokemon details and replaces any existing one or adds it to the bottom of the list.
   * @param id
   */
  getPokemonDetails(id: number) {
   this.pokemonApiService.getPokemonDetails(id).subscribe(pokemon => {
     const newPokemon = new DetailedPokemon(pokemon);
     // If pokemon already exists, replace it. Otherwise add new pokemon to list.
     if (this.pokemonList.find(pm => pm.id === newPokemon.id)) {
       this.pokemonList = this.pokemonList.map(pm => {
         if (pm.id === newPokemon.id) {
           return newPokemon;
         } else {
           return pm;
         }
       });
     } else {
       this.pokemonList = this.pokemonList.concat(newPokemon);
     }
     return newPokemon;
   });
  }
}

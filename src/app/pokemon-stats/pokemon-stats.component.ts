import {Component, Input, OnInit} from '@angular/core';
import {Ability, Stat} from '../assets/scripts/pokemon-helper';

@Component({
  selector: 'app-pokemon-stats',
  templateUrl: './pokemon-stats.component.html',
  styleUrls: ['./pokemon-stats.component.scss']
})
export class PokemonStatsComponent implements OnInit {
  @Input() weight: number;
  @Input() height: number;
  @Input() baseStats: Stat[];
  @Input() abilities: Ability[];

  constructor() { }

  ngOnInit() {
  }

}
